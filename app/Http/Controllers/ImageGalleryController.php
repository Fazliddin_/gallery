<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\ImageGallery;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File as LaraFile;



class ImageGalleryController extends Controller

{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    public function index()

    {
        $images = ImageGallery::get();

        return view('image-gallery',compact('images'));

    }

    public function upload(Request $request)

    {
        $this->validate($request, [
//    		'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,psd,webP|max:8192',
        ]);
//dd(12);
        $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $input['image']);
        ImageGallery::create($input);
        return back()
            ->with('success','Image Uploaded successfully.');
    }

    public function edit($image){
        // create Image from file
        $img = Image::make('images/' . $image)->rotate(-90);

        $img->save('images/' . $image);

        $image = ImageGallery::where('image', $image)->firstOrFail();

        $image->touch();
        return redirect()->back();
    }


    public function downloadImage($id){
        $image = ImageGallery::where('image', $id)->firstOrFail();
        $path = public_path() . '/images/'. $image->image;
        return response()->download($path, $image
            ->original_filename, ['Content-Type' => $image->mime]);
    }


    public function destroy($id)

    {

        $file = ImageGallery::find($id);
        $path = public_path().'/images/';

        if (!File::delete($path.$file->image))
        {
            Session::flash('success', 'ERROR deleted the File!');
            return redirect()->to('image-gallery');
        }
        else
        {
            $file->delete();
            Session::flash('success', 'Successfully deleted the File!');
            return redirect()->to('image-gallery');
        }
    }

    public function show()
    {
        $images = ImageGallery::get();

        return view('welcome',compact('images'));
    }

}
