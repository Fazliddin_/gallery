<?php

use Intervention\Image\Facades\Image;

use Illuminate\Support\Facades\Auth;


Route::get('/logout', function (){
    Auth::logout();
    return redirect('/');
});
Route::auth();


Route::get('/', 'ImageGalleryController@show');


Route::get('image-gallery', [
    'middleware' => ['auth'],
    'uses' => function () {
    }]);

Route::get('image-gallery', 'ImageGalleryController@index');

Route::post('image-gallery', 'ImageGalleryController@upload');

Route::delete('image-gallery/{id}', 'ImageGalleryController@destroy');

Route::get('image-gallery/{image}/edit', 'ImageGalleryController@edit')->name('edit');

Route::get('image-gallery/{image}',[
    'as' => 'd-image-gallery', 'uses' => 'ImageGalleryController@downloadImage']);
